# -*- coding: utf-8 -*-
import asyncio
import re

import aiohttp
import muffin

from . import app
from .utils import cache_async_result


@cache_async_result
@asyncio.coroutine
def get_quotation(currency):
    quotation_config = app.cfg.QUOTATIONS.get(currency.lower())
    if not quotation_config:
        raise aiohttp.errors.HttpBadRequest(message='invalid currency')
    url, regex = quotation_config
    response = yield from aiohttp.request('GET', url)
    response = yield from response.text()
    quotation = re.search(regex, response)
    return quotation.group(1)


@app.register('/api/{currency}/')
def quotation(request):
    quotation = yield from get_quotation(
        request.match_info.get('currency')
    )
    return {'quotation': quotation}
