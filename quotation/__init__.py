# -*- coding: utf-8 -*-
import muffin

from .middlewares import handler_error_middleware


app = muffin.Application(
    'quotation',
    middlewares=[handler_error_middleware],
    CONFIG='quotation.config.production',
)

from .views import *  # noqa
