# -*- coding: utf-8 -*-

# plugins config
PLUGINS = (
    'muffin_redis',
)

REDIS_HOST = '127.0.0.1'
REDIS_PORT = 6379


# app config
SECONDS = 1
MINUTES = 60 * SECONDS
HOURS = 60 * MINUTES
DAYS = 24 * HOURS

CACHE_DURATION = 2 * HOURS

REGEX_BASE = r'<input type="text" id="nacional" value="(?P<quote>[^"]+)"/>'
QUOTATIONS = {
    'dolar': ('http://dolarhoje.com/', REGEX_BASE),
    'libra': ('http://librahoje.com/', REGEX_BASE),
    'euro': ('http://eurohoje.com/', REGEX_BASE)
}
