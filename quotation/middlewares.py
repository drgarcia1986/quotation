# -*- coding: utf-8 -*-
import asyncio
import json

from aiohttp.errors import HttpProcessingError
import muffin


@asyncio.coroutine
def handler_error_middleware(app, handler):
    @asyncio.coroutine
    def middleware(request):
        try:
            return (yield from handler(request))
        except HttpProcessingError as e:
            return muffin.Response(
                text=json.dumps({'error': e.message}),
                status=e.code,
                content_type='application/json'
            )
    return middleware
