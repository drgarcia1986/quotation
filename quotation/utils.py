# -*- coding: utf-8 -*-
import asyncio

from . import app


def cache_async_result(func):

    @asyncio.coroutine
    def wrap(*args, **kwargs):
        cache_key = '{}-{}-{}'.format(
            func.__name__,
            ''.join(args),
            '|'.join(['{}={}'.format(k, v) for k, v in kwargs.items()])
        )

        result = yield from app.ps.redis.get(cache_key)
        if result:
            return result

        result = yield from func(*args, **kwargs)
        yield from app.ps.redis.set(
            cache_key, result, expire=app.cfg.CACHE_DURATION
        )

        return result

    return wrap
